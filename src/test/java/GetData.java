import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.get;

public class GetData {

    @Test
    public void testResponseCode() {

        // Response resp = RestAssured.get("https://reqres.in/api/users?page=2");
        //  int code = resp.getStatusCode();
        // with the static at class level there is no need for RestAssured

        int code = get("https://reqres.in/api/users?page=2").getStatusCode();
              
        System.out.println("status code is " + code);
        Assertions.assertEquals(200, code);
    }

    @Test
    public void testResponseBody() {
        Response resp = get("https://reqres.in/api/users?page=2");
        String data = resp.asString();
        System.out.println("Data is : " + data);

    }
}
